import 'package:flutter/material.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:intl/intl.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Calender'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Map<DateTime, List> _visibleEvents;
  var inputString = new DateFormat("dd/MM/yyyy").parse("10/05/2019");
  var inputString2 = new DateFormat("dd/MM/yyyy").parse("17/05/2019");
  var selectedDay;
  List<String> _bookedDays = ['Friday'];
  List<int> _bookedDaysInt = List();
  List _selectedEvents;
  DateTime _selectedDay;
  Map<DateTime, List> _events;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _selectedDay = DateTime.now();
    //print(inputString.weekday);
    _events = {
      inputString: ['Ex-1','Ex-2'],
      inputString2: ['Cx-1','Cx-2'],
    };
    _selectedEvents = _events[_selectedDay] ?? [];
    _visibleEvents = _events;
  }

  @override
  Widget build(BuildContext context) {
    _bookedDays.forEach((v) => _bookedDaysInInt(v));
    print(_bookedDaysInt);
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            _buildTableCalendar(_bookedDaysInt),
            const SizedBox(height: 8.0),
            Expanded(child: _buildEventList()),
          ],
        ));
  }

  Widget _buildTableCalendar(List<int> bookedDays) {
    return TableCalendar(
      locale: 'en_US',
      events: _visibleEvents,
      builders: CalendarBuilders(
        dayBuilder: (context, date, _) {
          return Container(
            margin: const EdgeInsets.all(4.0),
            padding: const EdgeInsets.only(top: 5.0, left: 6.0),
            color: Colors.transparent,
            child: isWorkingDay(date,bookedDays)
                ? _notifyDate(date)
                : Center(
                    child: Text(
                    '${date.day}',
                    style: TextStyle().copyWith(fontSize: 16.0),
                  )),
          );
        },
      ),
      initialCalendarFormat: CalendarFormat.month,
      formatAnimation: FormatAnimation.slide,
      startingDayOfWeek: StartingDayOfWeek.monday,
      availableGestures: AvailableGestures.all,
      availableCalendarFormats: const {
        CalendarFormat.month: 'Month',
        CalendarFormat.twoWeeks: '2 weeks',
        CalendarFormat.week: 'Week',
      },
      calendarStyle: CalendarStyle(
        holidayStyle: TextStyle(color: Colors.amber),
        selectedColor: Colors.lightBlueAccent[200],
        todayColor: Colors.deepPurple[200],
        markersColor: Colors.red[700],
      ),
      onDaySelected: _onDaySelected,
      onVisibleDaysChanged: _onVisibleDaysChanged,
    );
  }
  Widget _buildEventList() {
    print(_selectedEvents);
    return ListView(
      children: _selectedEvents
          .map((event) => Container(
        decoration: BoxDecoration(
          color: Colors.green,
          border: Border.all(width: 0.8),
          borderRadius: BorderRadius.circular(12.0),
        ),
        margin: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
        child: ListTile(
          title: Text(event.toString()),
          onTap: () => print('$event tapped!'),
        ),
      )).toList(),
    );
  }

  void _onDaySelected(DateTime day, List events) {
    setState(() {
      _selectedDay = day;
      _selectedEvents = events;
    });
  }

  isWorkingDay(DateTime date,List<int> bookedDays) {
      //bookedDays.forEach((v) => print(v));
    print(bookedDays.length);
    switch(bookedDays.length){
      case 1:
        if(date.weekday == bookedDays[0]){
          return true;
        }else{
          return false;
        }
        break;
      case 2:
        if(date.weekday == bookedDays[0] || date.weekday == bookedDays[1]){
          return true;
        }else{
          return false;
        }
        break;
      case 3:
        if(date.weekday == bookedDays[0] || date.weekday == bookedDays[1] || date.weekday == bookedDays[2]){
          return true;
        }else{
          return false;
        }
        break;
      case 4:
        if(date.weekday == bookedDays[0] || date.weekday == bookedDays[1] || date.weekday == bookedDays[2] || date.weekday == bookedDays[3]){
          return true;
        }else{
          return false;
        }
        break;
      case 5:
        if(date.weekday == bookedDays[0] || date.weekday == bookedDays[1] || date.weekday == bookedDays[2] || date.weekday == bookedDays[3] || date.weekday == bookedDays[4]){
          return true;
        }else{
          return false;
        }
        break;
      case 6:
        if(date.weekday == bookedDays[0] || date.weekday == bookedDays[1] || date.weekday == bookedDays[2] || date.weekday == bookedDays[3] || date.weekday == bookedDays[4] || date.weekday == bookedDays[5]){
          return true;
        }else{
          return false;
        }
        break;
      case 7:
        if(date.weekday == bookedDays[0] || date.weekday == bookedDays[1] || date.weekday == bookedDays[2] || date.weekday == bookedDays[3] || date.weekday == bookedDays[4] || date.weekday == bookedDays[5] || date.weekday == bookedDays[6]){
          return true;
        }else{
          return false;
        }
        break;
    }
  }
  _notifyDate(DateTime date) {
    return Center(
        child: Container(
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: Colors.lightBlueAccent
          ),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
      Center(child: Text(
            '${date.day}',
            style: TextStyle().copyWith(fontSize: 16.0),
          ),),
/*
      Container(
            width: 8.0,
            height: 8.0,
            margin: const EdgeInsets.symmetric(horizontal: 0.3),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Colors.red,
            )),
*/
    ]),
        ));
  }

  _bookedDaysInInt(String v) {
    switch(v.toLowerCase()){
      case 'monday':
        _bookedDaysInt.add(1);
        break;
      case 'tuesday':
        _bookedDaysInt.add(2);
        break;
      case 'wednesday':
        _bookedDaysInt.add(3);
        break;
      case 'thursday':
        _bookedDaysInt.add(4);
        break;
      case 'friday':
        _bookedDaysInt.add(5);
        break;
      case 'saturday':
        _bookedDaysInt.add(6);
        break;
      case 'sunday':
        _bookedDaysInt.add(7);
        break;
    }
  }
  void _onVisibleDaysChanged(DateTime first, DateTime last, CalendarFormat format) {
    setState(() {
      _visibleEvents = Map.fromEntries(
        _events.entries.where(
              (entry) =>
          entry.key.isAfter(first.subtract(const Duration(days: 1))) &&
              entry.key.isBefore(last.add(const Duration(days: 1))),
        ),
      );
    });
  }
}


