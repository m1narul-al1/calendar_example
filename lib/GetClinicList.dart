import 'package:http/http.dart' as http;
import 'dart:convert';

class CommonDataReturn {
  final String status;
  final String faildReson;
  final String code;

  CommonDataReturn({this.status, this.faildReson, this.code});

  factory CommonDataReturn.fromJson(Map<String, dynamic> json) {
    return CommonDataReturn(
      status: json['Status'],
      faildReson: json['FaildReson'],
      code: json['Code'],
    );
  }
}

class GetClinicList {
  List<ClinicListWithTiming> clinicListWithTiming;

  GetClinicList({
    this.clinicListWithTiming,
  });

  factory GetClinicList.fromJson(Map<String, dynamic> json) =>
      new GetClinicList(
        clinicListWithTiming: new List<ClinicListWithTiming>.from(
            json["clinicListWithTiming"]
                .map((x) => ClinicListWithTiming.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "clinicListWithTiming":
            new List<dynamic>.from(clinicListWithTiming.map((x) => x.toJson())),
      };
}

class ClinicListWithTiming {
  int clinicId;
  String clinicName;
  Timings timings;

  ClinicListWithTiming({
    this.clinicId,
    this.clinicName,
    this.timings,
  });

  factory ClinicListWithTiming.fromJson(Map<String, dynamic> json) =>
      new ClinicListWithTiming(
        clinicId: json["clinicID"],
        clinicName: json["clinicName"],
        timings: Timings.fromJson(json["timings"]),
      );

  Map<String, dynamic> toJson() => {
        "clinicID": clinicId,
        "clinicName": clinicName,
        "timings": timings.toJson(),
      };
}

class Timings {
  String day;
  String timeFromMorning;
  String timeToMorning;
  String timeFromDay;
  String timeToDay;
  dynamic timeFromNight;
  dynamic timeToNight;
  var currentTime;
  var securityID;

  Timings({
    this.day,
    this.timeFromMorning,
    this.timeToMorning,
    this.timeFromDay,
    this.timeToDay,
    this.timeFromNight,
    this.timeToNight,
  });

  factory Timings.fromJson(Map<String, dynamic> json) => new Timings(
        day: json["day"],
        timeFromMorning:
            json["timeFromMorning"] == null ? null : json["timeFromMorning"],
        timeToMorning:
            json["timeToMorning"] == null ? null : json["timeToMorning"],
        timeFromDay: json["timeFromDay"] == null ? null : json["timeFromDay"],
        timeToDay: json["timeToDay"] == null ? null : json["timeToDay"],
        timeFromNight: json["timeFromNight"],
        timeToNight: json["timeToNight"],
      );

  Map<String, String> toJson() => {
        "currentTime": currentTime,
      };

  Map<String, String> toHeader() => {
        "securityID": securityID,
      };
}

Future<dynamic> fetchClinicList({String securityID, var currentTime}) async {
  print('$securityID : $currentTime');
  var body = json.encode({"currentTime": currentTime});
  var uri = Uri.http("cure-staging-api.azurewebsites.net",
      Uri.encodeFull("/api/doctor/GetClinicList"));
  print(uri);
  final response = await http.post(uri,
      headers: {"Content-Type":"application/json","SecurityID": securityID},
      body: body);
  //print(response.body);
  if (response.statusCode == 200) {
    // If server returns an OK response, parse the JSON
    if(jsonDecode(response.body)["FaildReson"] != "No Data Found"){
      if(json.decode(response.body)["clinicListWithTiming"] != null){
        List getClinicList = json.decode(response.body)["clinicListWithTiming"];
        //print(getClinicList);
        List<ClinicListWithTiming> clinicList = createClinicList(getClinicList);
        //print(clinicList);
        return clinicList;
      }
    }else{
        return CommonDataReturn.fromJson(json.decode(response.body));
      }
  } else {
    // If that response was not OK, throw an error.
    throw Exception('Failed to load post');
  }
}

List<ClinicListWithTiming> createClinicList(List getClinicList) {
  List<ClinicListWithTiming> list = new List();
  for (int i = 0; i < getClinicList.length; i++) {
    ClinicListWithTiming model = ClinicListWithTiming.fromJson(getClinicList[i]);
    //print(model.clinicId);
    list.add(model);
  }

  return list;
}


