import 'package:flutter/material.dart';
import 'package:flutter_app_calender/GetClinicList.dart';
import 'package:flutter_app_calender/GetDistinctDays.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:intl/intl.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Calender'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var inputString = new DateFormat("dd/MM/yyyy").parse("10/05/2019");
  var inputString2 = new DateFormat("dd/MM/yyyy").parse("17/05/2019");
  var selectedDay;
  List<String> _bookedDays;
  List<int> _bookedDaysInt = List();
  CommonDataReturn _noEvents;
  DateTime _selectedDay;
  var getDistinctDaysResponse;
  var getClinicList;
  List<ClinicListWithTiming> _events;
  var securityID = 'jf9KeTQULfYVOxgIByuNgA';

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _selectedDay = DateTime.now();
    if (getDistinctDaysResponse == null) {
      getDistinctDaysResponse =
          fetchPostDays(securityID: securityID).then((result) {
        print(result);
        setState(() {
          _bookedDays = result;
          _bookedDays?.forEach((v) => _bookedDaysInInt(v));
        });
      });
    }
    if (getClinicList == null) {
      getClinicList = fetchClinicList(
              currentTime: _selectedDay.toString(), securityID: securityID)
          .then((result) {
        print(result);
        if (result is CommonDataReturn) {
          setState(() {
            _noEvents = result;
          });
          //print(result.faildReson);
        }
        if (result is List<ClinicListWithTiming>) {
          setState(() {
            _events = result;
          });
          //print(result.clinicId);
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            _buildTableCalendar(_bookedDaysInt),
            const SizedBox(height: 8.0),
            Expanded(
                child: _events != null
                    ? _buildEventList()
                    : _noEvents != null
                        ? _buildNoEvent()
                        : Center(
                            child: Text('Loading...'),
                          )),
          ],
        ));
  }

  Widget _buildTableCalendar(List<int> bookedDays) {
    return TableCalendar(
      locale: 'en_US',
      builders: CalendarBuilders(
        todayDayBuilder: (context, date, _) {
          return Container(
              margin: const EdgeInsets.all(4.0),
          padding: const EdgeInsets.only(top: 5.0, left: 6.0),
          color: Colors.transparent,
          child: _buildCurrentDay(date),
          );
        },
        selectedDayBuilder: (context, date, _) {
          return Container(
            margin: const EdgeInsets.all(4.0),
            padding: const EdgeInsets.only(top: 5.0, left: 6.0),
            color: Colors.transparent,
            child: _buildSelectedDay(date),
          );
        },
        dayBuilder: (context, date, _) {
          return Container(
            margin: const EdgeInsets.all(4.0),
            padding: const EdgeInsets.only(top: 5.0, left: 6.0),
            color: Colors.transparent,
            child: isWorkingDay(date, bookedDays)
                ? _notifyDate(date)
                : Center(
                    child: Text(
                    '${date.day}',
                    style: TextStyle().copyWith(fontSize: 16.0),
                  )),
          );
        },
      ),
      initialCalendarFormat: CalendarFormat.month,
      formatAnimation: FormatAnimation.slide,
      startingDayOfWeek: StartingDayOfWeek.monday,
      availableGestures: AvailableGestures.all,
      headerStyle:
          HeaderStyle(centerHeaderTitle: true, formatButtonVisible: false),
      calendarStyle: CalendarStyle(
        holidayStyle: TextStyle(color: Colors.amber),
        selectedColor: Colors.lightBlueAccent[200],
        todayColor: Colors.deepPurple[200],
        markersColor: Colors.red[700],
      ),
      selectedDay: selectedDay,
      onDaySelected: (date, events) {
        _onDaySelected(date, events);
      },
    );
  }

  isWorkingDay(DateTime date, List<int> bookedDays) {
    switch (bookedDays.length) {
      case 1:
        if (date.weekday == bookedDays[0]) {
          return true;
        } else {
          return false;
        }
        break;
      case 2:
        if (date.weekday == bookedDays[0] || date.weekday == bookedDays[1]) {
          return true;
        } else {
          return false;
        }
        break;
      case 3:
        if (date.weekday == bookedDays[0] ||
            date.weekday == bookedDays[1] ||
            date.weekday == bookedDays[2]) {
          return true;
        } else {
          return false;
        }
        break;
      case 4:
        if (date.weekday == bookedDays[0] ||
            date.weekday == bookedDays[1] ||
            date.weekday == bookedDays[2] ||
            date.weekday == bookedDays[3]) {
          return true;
        } else {
          return false;
        }
        break;
      case 5:
        if (date.weekday == bookedDays[0] ||
            date.weekday == bookedDays[1] ||
            date.weekday == bookedDays[2] ||
            date.weekday == bookedDays[3] ||
            date.weekday == bookedDays[4]) {
          return true;
        } else {
          return false;
        }
        break;
      case 6:
        if (date.weekday == bookedDays[0] ||
            date.weekday == bookedDays[1] ||
            date.weekday == bookedDays[2] ||
            date.weekday == bookedDays[3] ||
            date.weekday == bookedDays[4] ||
            date.weekday == bookedDays[5]) {
          return true;
        } else {
          return false;
        }
        break;
      case 7:
        if (date.weekday == bookedDays[0] ||
            date.weekday == bookedDays[1] ||
            date.weekday == bookedDays[2] ||
            date.weekday == bookedDays[3] ||
            date.weekday == bookedDays[4] ||
            date.weekday == bookedDays[5] ||
            date.weekday == bookedDays[6]) {
          return true;
        } else {
          return false;
        }
        break;
      default:
        return false;
    }
  }

  _notifyDate(DateTime date) {
    return Center(
        child: Container(
      decoration:
          BoxDecoration(shape: BoxShape.circle, color: Colors.lightBlueAccent),
      child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Center(
              child: Text(
                '${date.day}',
                style: TextStyle().copyWith(fontSize: 16.0),
              ),
            ),
          ]),
    ));
  }

  void _onDaySelected(DateTime day, List events) {
    setState(() {
      _selectedDay = day;
      getClinicList = fetchClinicList(
              currentTime: _selectedDay.toString(), securityID: securityID)
          .then((result) {
        print(result);
        if (result is CommonDataReturn) {
          setState(() {
            _noEvents = result;
            _events = null;
          });
          //print(result.faildReson);
        }
        if (result is List<ClinicListWithTiming>) {
          setState(() {
            _events = result;
            _noEvents = null;
          });
          //print(result.clinicId);
        }
      });

      //_selectedEvents = events;
      print(_selectedDay);
      print(_bookedDaysInt);
    });
  }

  _bookedDaysInInt(String v) {
    switch (v.toLowerCase()) {
      case 'mon':
        _bookedDaysInt.add(1);
        break;
      case 'tue':
        _bookedDaysInt.add(2);
        break;
      case 'wed':
        _bookedDaysInt.add(3);
        break;
      case 'thu':
        _bookedDaysInt.add(4);
        break;
      case 'fri':
        _bookedDaysInt.add(5);
        break;
      case 'sat':
        _bookedDaysInt.add(6);
        break;
      case 'sun':
        _bookedDaysInt.add(7);
        break;
    }
  }

  Widget _buildSelectedDay(DateTime date) {
    return Center(
        child: Container(
          decoration:
          BoxDecoration(shape: BoxShape.circle, color: Colors.deepPurple),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Center(
                  child: Text(
                    '${date.day}',
                    style: TextStyle().copyWith(fontSize: 16.0),
                  ),
                ),
              ]),
        ));
  }
  Widget _buildCurrentDay(DateTime date) {
    return Center(
        child: Container(
          decoration:
          BoxDecoration(shape: BoxShape.circle, color: Colors.greenAccent),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Center(
                  child: Text(
                    '${date.day}',
                    style: TextStyle().copyWith(fontSize: 16.0),
                  ),
                ),
              ]),
        ));
  }
  _buildNoEvent() {
    return Center(
      child: Text('No Event found'),
    );
  }
  Widget _buildEventList() {
    return ListView.builder(
      itemCount: _events.length,
      itemBuilder: (BuildContext context, int index) {
        return Container(
          decoration: BoxDecoration(
            color: (index % 2 == 0)
                ? Colors.lightBlueAccent
                : Colors.deepPurpleAccent,
            border: Border.all(width: 0.8),
            borderRadius: BorderRadius.circular(12.0),
          ),
          margin: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
          child: ListTile(
            title: Text(_events[index].clinicName),
            onTap: () => print('${_events[index].clinicName} tapped!'),
          ),
        );
      },
    );
  }
}
